FROM golang:1.22.1-alpine3.19 AS build

WORKDIR /build
COPY . .

RUN go build .

FROM scratch

WORKDIR /opt/squirrel-backend
COPY --from=build /build/squirrel-backend .

RUN useradd squirrel
RUN chown squirrel:squirrel -R .
USER squirrel

CMD ./squirrel-backend
