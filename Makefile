BUILD_DIR="./dist"

run: main.go
	go run main.go

build: main.go
	go build -o $(BUILD_DIR) main.go

clean:
	go clean
